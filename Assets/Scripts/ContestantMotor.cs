using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContestantMotor : MonoBehaviour
{
    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float turnSpeed = 10f;
    [SerializeField]
    private LayerMask layerGrounds;

    private Rigidbody rigid;
    private Animator anim;

    private bool isGrounded;
    private bool isMoving;
    private int currentPlatform = -1;
    private float turnDirection;
    private Vector3 lastJumpForce;
    private Vector3 lastVelocity;

    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        CheckIsGrounded();
        Move();
    }

    private void OnCollisionEnter(Collision collision)
    {
        var platform = collision.collider.gameObject.GetComponentInParent<Platform>();
        if (platform != null)
        {
            Jump(platform.GetForce());
        }
        currentPlatform++;
    }

    private void CheckIsGrounded()
    {
        var origin = transform.position;
        origin.y += 0.1f;
        isGrounded = Physics.Raycast(origin, Vector3.down, 0.2f, layerGrounds);
        anim.SetBool("IsGrounded", isGrounded);
    }

    private void Jump(float force, int animIndexResult = - 1)
    {
        lastJumpForce = Vector3.up * force;
        Vector3 directionForce = lastJumpForce;
        rigid.AddForce(directionForce, ForceMode.Impulse);
       
        if(animIndexResult == -1)
            animIndexResult = Random.Range(1, 3);
        switch (animIndexResult)
        {
            case 0:
                anim.SetTrigger("Jump");
                break;
            case 1:
                anim.SetTrigger("ForwardFlip");
                break;
            case 2:
                anim.SetTrigger("BackFlip");
                break;
        }
    }

    private void Move()
    {
        if (isMoving && !isGrounded)
        {
            var velocity = transform.forward * speed;
            velocity.y = rigid.velocity.y;
            rigid.velocity = velocity;
            rigid.angularVelocity = Vector3.up * turnDirection * turnSpeed;
            lastVelocity = rigid.velocity;
        }
        else
        {
            rigid.velocity = Vector3.Lerp(rigid.velocity, new Vector3(0, rigid.velocity.y, 0), 2f * Time.deltaTime);
            rigid.angularVelocity = Vector3.zero;
        }
    }

    public void SetTurnDir(float turnDirection) => this.turnDirection = turnDirection;
    public void SetIsMoving(bool value) => isMoving = value;

    public Vector3 GetLastJumpForce() => lastJumpForce;
    public bool IsGrounded() => isGrounded;
    public int GetCurrentPlatform() => currentPlatform;

    public void EnablePhysics(bool isEnabled) => rigid.isKinematic = !isEnabled;
}
