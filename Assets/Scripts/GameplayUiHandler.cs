using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayUiHandler : MonoBehaviour
{
    public static GameplayUiHandler Instance;

    public delegate void OnClickedStart();
    public event OnClickedStart onClickedStart;

    [SerializeField, Header("UiReferences")]
    private Button buttonStart;
    [SerializeField]
    private TextMeshProUGUI textCurrentLevel;
    [SerializeField]
    private TextMeshProUGUI textNextLevel;
    [SerializeField]
    private Image progressBarLevel;

    [SerializeField]
    private RectTransform panelLevelCompleted;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Start()
    {
        buttonStart.gameObject.SetActive(true);
        panelLevelCompleted.gameObject.SetActive(false);
        buttonStart.onClick.AddListener(OnClickedButtonStart);
    }

    private void OnClickedButtonStart()
    {
        onClickedStart?.Invoke();
        buttonStart.gameObject.SetActive(false);
    }

    public void ShowLevelComplete() => panelLevelCompleted.gameObject.SetActive(true);

    public void SetTextLevel(int currentLevel)
    {
        textCurrentLevel.text = currentLevel.ToString();
        textNextLevel.text = currentLevel++.ToString();
    }

    public void SetProgressbarLevel(float value) => progressBarLevel.fillAmount = value;
}
