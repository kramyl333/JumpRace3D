using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public static float Horizontal { private set; get; }
    public static float Vertical { private set; get; }
    public static bool IsTouching { private set; get; }

    private Vector2 startDragPos;
    private Vector2 lastDragPos;

    public void OnBeginDrag(PointerEventData eventData)
    {
        startDragPos = Camera.main.ScreenToViewportPoint(eventData.position);
        lastDragPos = startDragPos;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 currentDragPos = Camera.main.ScreenToViewportPoint(eventData.position);
        if(lastDragPos.magnitude != currentDragPos.magnitude)
        {
            Debug.Log(currentDragPos + " - " + startDragPos);
            var dragPos = currentDragPos - startDragPos;
            if (dragPos.magnitude > 0.05f)
            {
                var dir = dragPos.normalized;
                Debug.Log(dir);
                Horizontal = dir.x;
                Vertical = dir.y;
            }
        }
        else
        {
            Horizontal = 0;
            Vertical = 0;
        }

        
        lastDragPos = currentDragPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        startDragPos = Vector2.zero;
        Horizontal = 0;
        Vertical = 0;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        IsTouching = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        IsTouching = false;
    }
}
