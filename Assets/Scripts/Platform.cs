using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField, Header("Settings")]
    private float force = 10;

    [SerializeField, Header("References")]
    private Animator animTrampoline;

    public float GetForce()
    {
        animTrampoline.SetTrigger("Bounce");
        return force;
    }
}
