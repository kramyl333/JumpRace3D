using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField, Header("References")]
    private ContestantMotor playerMotor;
    void Start()
    {
        GameplayUiHandler.Instance.onClickedStart += OnClickedStart;
        playerMotor.EnablePhysics(false);
    }
    void Update()
    {
        playerMotor.SetIsMoving(InputHandler.IsTouching);

#if UNITY_EDITOR
        var inputDir = Input.GetAxis("Horizontal");
        if (inputDir == 0)
            inputDir = InputHandler.Horizontal;
#elif UNITY_ANDROID
        var inputDir = InputHandler.Horizontal;
#endif
        playerMotor.SetTurnDir(inputDir);
        GameplayUiHandler.Instance.SetProgressbarLevel((float)playerMotor.GetCurrentPlatform() / 8);
        if (playerMotor.GetCurrentPlatform() == 8)
        {
            GameplayUiHandler.Instance.ShowLevelComplete();
        }
    }

    private void OnClickedStart()
    {
        playerMotor.EnablePhysics(true);
    }

}
